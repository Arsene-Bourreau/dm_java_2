import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        Integer res = a+b;
        return res;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer res = null;
        for(Integer nb : liste){
            if(res==null){res=nb;}
            if(nb<res){res=nb;}
        }
        return res;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T mot:liste){
            if(valeur.compareTo(mot) >= 0){
            return false;}
        }
        return true;
  }




    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res = new ArrayList<>();
        int x = 0;
        int y = 0;
        while (x < liste1.size() && y < liste2.size())
        {
            if (liste1.get(x).equals(liste2.get(y)))
            {
                if(!res.contains(liste1.get(x)))
                {res.add(liste1.get(x));}
                x++;
                y++;
            }
            else{
                if (liste1.get(x).compareTo(liste2.get(y)) > 0)
                    {y++;}
                else
                    {x++;}
                }
            }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<>();
        String[] mots = texte.split(" ");
        for (String charac : mots){
            if(!charac.equals("")){res.add(charac);}
        }
        return res;
    }



    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        Map<String, Integer> dico = new HashMap<>();
        String res;
        for(String mot : decoupe(texte)){
        if(dico.containsKey(mot))
            dico.put(mot, dico.get(mot)+1);
        else
            dico.put(mot, 1);
        }
        if(dico.size() > 0){
        Map.Entry<String, Integer> max = null;
        for (Map.Entry<String, Integer> val : dico.entrySet()) {
            if (max == null || val.getValue().compareTo(max.getValue()) == 0 && val.getKey().compareTo(max.getKey()) < 0)
                max = val;
            if (max == null || val.getValue().compareTo(max.getValue()) > 0)
                max = val;
        }
        res = max.getKey();
        return res;
        }
        else
        {return null;}
     }

    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        List<String> liste = new ArrayList<>();
        List<String> res = new ArrayList<>();
        String val[]  = chaine.split("");
        for (String elem : val){
          if(elem.equals("(") || elem.equals(")"))
            liste.add(elem);}
        if(liste.size() >= 1){
          int ouvert = 0;
          for(int i = 0; i < liste.size(); i++){
            if(liste.get(i).equals("(")){
              ouvert++;}
            else if(liste.get(i).equals(")")){
              if(ouvert >= 1)
                {ouvert--;}
              else
                {return false;}
            }
          }
          if(ouvert == 0)
            {return true;}
          else
            {return false;}
        }
        return true;
      }
    

    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        if (chaine.isEmpty())
            return true;
        int nbPar=0;
        int nbCro=0;
        ArrayList<String> dernierOuvert=new ArrayList<>();
        for (char c : chaine.toCharArray()){
            if(c =='('){
                dernierOuvert.add("P");
                nbPar+=1;
            }
            if(c==')'){
                if (dernierOuvert.size()==0 ||!dernierOuvert.get(dernierOuvert.size()-1).equals("P") || nbPar==0){
                    return false;
                }
                nbPar-=1;
                dernierOuvert.remove(dernierOuvert.size()-1);
            }
            if (c=='['){
                dernierOuvert.add("C");
                nbCro+=1;
            }
            if (c==']'){
                if (dernierOuvert.size()==0 ||!dernierOuvert.get(dernierOuvert.size()-1).equals("C")|| nbCro==0){
                    return false;
                }
                nbCro-=1;
                dernierOuvert.remove(dernierOuvert.size()-1);
            }
        }
        return dernierOuvert.size() == 0;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int debut = 0;
        int fin = liste.size();
        int milieu;
        while (fin-debut >= 1)
        {
            milieu = (debut + fin) / 2;
            if (liste.get(milieu) == valeur)
                return true;
            if (liste.get(milieu) > valeur)
                fin = milieu;
            else
                debut = milieu;
        }
        return false;
    }
    



}
